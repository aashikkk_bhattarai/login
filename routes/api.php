<?php
use App\Http\Middleware;
use App\Models\Post;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Passwrod;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', function (Request $request){
    $email = $request->email;
    $password=$request->password;
    $hashedPassword=Hash::make($passsword, ['rounds'=> 10]);
    $user=new User;
    $user->email=$email;
    $user->password=$hashedPassword;
    $user->save();
    return response()->json(["user"=>$user]);
});

Route::post('/login', function (Request $request){
    $email=$request->email;
    $password=$request->password;
    $user=user::where('email',$email)->first();
    if(!Hash::check($password,$user->passsword)){
        return response()->json(["success"=> false,"message"=>"password does not match"],401);
    }
    $token=$user->createToken('token-name');
    return response()->json(["token"=>$token->plainTextToken]);
});

Route::middleware('auth:sanctum')->get('/protected-route' function (){
    return response()->json(["messege"=> "this is protected route, only authenticated user can access"]);
}];

Route::middleware('auth:sanctum')->post('/create-post', function(Request $request){
    $title=request->title;
    $description=$request->description;
    $user=User::find($request->user())->first();
    $post=new Post(["title"=>$title,"description" =>$description]);
    $user->post()->save($post);
    return response()->json(["post"=> $post],201);
});

Route::middleware('auth:santum')->get('/fetch-post/{id}', function (Request $request, $id){
    $post=Post::find($id)->first();
    if ($request->user()->id !=$post->user_id){
        return response()->json(["success"=>false, 'messege' => "forbidden request"], 401);
    }
    $post->delete();
    return response()->json(["success" => true, "messege" => "post deleted"]);
    });
    










Route::post('forget-passsword', function (Request $request) {
    $request->validate->validate(['email'=>'required|email']);
    $status=Password::sendResetLink(
        $request->only('email') 
    );
  return $status === Password::RESET_LINK_SENT
  ?back()->with(['status'=>__($status)])
  :back()->withErrors(['email'=__($status)]);
})->middleware(['guest']})->name('password.email');


})


